import React from 'react';
import { BrowserRouter, Route} from 'react-router-dom'
//import './App.css';
import HeadeBar from './Componends/Header'
import HomePage from './Componends/HomePage';
import Footer from './Componends/Footer';
import Companies from './Componends/Companies';
import CompanyProfile from './Componends/CompanyProfile';
import Projects from './Componends/Projects';
import MyProfile from './Componends/MyProfile';
import FriendsProfiles from './Componends/FriendsProfiles';
import Jobs from './Componends/Jobs';
import Messages from './Componends/Messages';
import Login from './Componends/Login';
import Setting from './Componends/Setting';

function App() {
  return (
    
    <div> 

      <HeadeBar/>
      
      <div class="page-wrapper">
				<div class="container-fluid">
					
            <BrowserRouter>

              <Route path='/home' component={HomePage}/>

              <Route path='/companies' component={Companies}/>

              <Route path='/companyprofile' component={CompanyProfile}/>

              <Route path='/projects' component={Projects}/>

              <Route path='/friendsprofiles' component={FriendsProfiles}/>

              <Route path='/myprofile' component={MyProfile}/>

              <Route path='/jobs' component={Jobs}/>

              <Route path='/messages' component={Messages}/>

              <Route path='/login' component={Login}/>

              <Route path='/setting' component={Setting}/>

            </BrowserRouter>

          <Footer/>

        </div>
      </div>
    </div>

  );
}

export default App;
