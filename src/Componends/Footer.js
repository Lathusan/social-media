import React from 'react'

function Footer() {
    return (

        <footer>
            <div class="footy-sec mn no-margin">
                <div class="container">
                    <ul>
                        <li><a href="help-center.html" title="">Help Center</a></li>
                        <li><a href="about.html" title="">About</a></li>
                        <li><a href="#" title="">Privacy Policy</a></li>
                        <li><a href="#" title="">Community Guidelines</a></li>
                        <li><a href="#" title="">Cookies Policy</a></li>
                        <li><a href="#" title="">Career</a></li>
                        <li><a href="forum.html" title="">Forum</a></li>
                        <li><a href="#" title="">Language</a></li>
                        <li><a href="#" title="">Copyright Policy</a></li>
                    </ul>
                    <p><img src="http://gambolthemes.net/workwise-new/images/copy-icon2.png" alt=""/>Copyright 2019</p>
                    <img class="fl-rgt" src="https://pbs.twimg.com/profile_images/896044730991222785/NX4t48gb_400x400.jpg" alt="" style={{height:'30px', width:'60px', marginBlockStart:'30px', marginInlineEnd:'8px'}}/>
                </div>
            </div>
        </footer>

    );
}

export default Footer;